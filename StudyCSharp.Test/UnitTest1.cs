using System;
using System.Linq;
using Xunit;

namespace StudyCSharp.Test
{
    public class UnitTest1
    {
        [Fact]
        public void DelegateTest()
        {
            var numbers = Enumerable.Range(1, 200).ToList();

            numbers.FindAll(num => num % 2 == 1);
            numbers.TrueForAll(n => n <= 50);
        }

        [Fact]
        public void MultiCastingTest()
        {
            Func<int> cb = () => 1;
            cb += () => 2;
            cb += () => 3;

            Assert.Equal(cb(), 3);
        }

        [Fact]
        public void Test1()
        {
            string thisCantBeNull;

            Assert.Equal(nameof(thisCantBeNull), "thisCantBeNull");
        }

        [Fact]
        public void UsingNullSafeOperator()
        {
            var eventHandler = new NullSafeOperatorEvent();

            eventHandler.Call();
        }
    }


    internal class NullSafeOperatorEvent
    {
        private EventHandler<int> Handlers;

        public void Call()
        {
            Handlers?.Invoke(this, 1);
        }
    }
}