﻿using FluentAssertions;
using StudyCSharp.Test.Fixture;
using Xunit;

namespace StudyCSharp.Test.Test
{
    public class EnumTest
    {
        private bool MultipleEnumTest(TestEnum enums)
        {
            return enums.HasFlag(TestEnum.A | TestEnum.B);
        }


        [Fact]
        public void Test()
        {
            MultipleEnumTest(TestEnum.A | TestEnum.B | TestEnum.C).Should().BeTrue();
        }
    }
}