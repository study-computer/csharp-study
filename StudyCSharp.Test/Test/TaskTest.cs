﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace StudyCSharp.Test.Test
{
    public class TaskTest
    {
        public TaskTest(ITestOutputHelper output)
        {
            Output = output;
        }

        private ITestOutputHelper Output { get; }

        [Fact]
        public void 쓰레드폴링이_과연_동작할까()
        {
            Output.WriteLine("StartTest");

            var tasks = Enumerable.Range(0, 20).Select(number => Task.Factory.StartNew(() =>
            {
                Thread.Sleep(1000);
                Output.WriteLine(DateTime.Now.ToString("O"));
            }));

            Output.WriteLine("TEST");
            Task.WaitAll(tasks.ToArray());
        }
    }
}