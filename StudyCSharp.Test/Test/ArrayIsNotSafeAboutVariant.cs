﻿using System;
using FluentAssertions;
using StudyCSharp.Test.Fixture;
using Xunit;

namespace StudyCSharp.Test.Test
{
    public class ArrayIsNotSafeAboutVariant
    {
        private static void UnsafeVariantArray(Base[] bases)
        {
            bases[0] = new DerivedA();
        }

        private static void CovariantTest(Base[] bases)
        {
            foreach (var item in bases) Console.WriteLine($"{item.Id} has a content as {item.Content}");
        }

        [Fact]
        public void SafeCase_UseAsParameter()
        {
            CovariantTest(new[] {new DerivedA()});
        }

        [Fact]
        public void UnsafeCase_AllocateIndexOtherDerivedType()
        {
            Action action = () =>
            {
                Base[] items = new DerivedA[5];

                items[0] = new DerivedB();
            };

            action.Should().Throw<Exception>();
        }

        [Fact]
        public void UnsafeCaseArrayParameter()
        {
            Action action = () => UnsafeVariantArray(new DerivedB[5]);

            action.Should().Throw<Exception>();
        }
    }
}