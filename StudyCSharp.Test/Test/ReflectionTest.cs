﻿using System;
using StudyCSharp.Test.Fixture;
using Xunit;

namespace StudyCSharp.Test.Test
{
    public class ReflectionTest
    {
        [Fact]
        public void can_we_create_instance_of_only_interface()
        {
            var instance = Activator.CreateInstance<IFake>();

            instance.NotImplementMethod();
        }
    }
}