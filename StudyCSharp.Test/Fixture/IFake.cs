﻿namespace StudyCSharp.Test.Fixture
{
    public interface IFake
    {
        string NotImplementMethod();
    }
}