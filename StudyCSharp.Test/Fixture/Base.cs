﻿using System;

namespace StudyCSharp.Test.Fixture
{
    public class Base : IComparable<Base>
    {
        public int Id { get; set; }
        public string Content { get; set; }

        public int CompareTo(Base other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            var idComparison = Id.CompareTo(other.Id);
            return idComparison != 0
                ? idComparison
                : string.Compare(Content, other.Content, StringComparison.Ordinal);
        }
    }
}