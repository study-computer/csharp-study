﻿using System;

namespace StudyCSharp.Test.Fixture
{
    [Flags]
    public enum TestEnum
    {
        A,
        B,
        C,
        D,
        E
    }
}