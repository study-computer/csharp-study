﻿using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CodingSkill.Chapter1
{
    [TestClass]
    public class Problem4
    {
        [TestMethod]
        public void Problem()
        {
            var result = "";
            var count = 0;

            Parallel.For(0, 10, _ =>
            {
                lock (result)
                {
                    result += ++count;
                }
            });

            result.Should().Be("12345678910");
        }
    }
}