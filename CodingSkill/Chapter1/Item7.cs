﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CodingSkill.Chapter1
{
    [TestClass]
    public class Item7
    {
        [TestMethod]
        public void Problem()
        {
            var result = int.TryParse("This is not number.", out var number)
                ? number
                : 123;

            result.Should().Be(123);
        }
    }
}