﻿using System;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CodingSkill.Chapter1
{
    [TestClass]
    public class Item6
    {
        private static string Result { get; set; } = "";

        [TestMethod]
        public void Problem()
        {
            using (new Class2())
            {
            }

            Result.Should().Be("Hello World!");
        }

        private class Class1 : IDisposable
        {
            public void Dispose()
            {
                Result += "Hello";
            }
        }

        private class Class2 : Class1, IDisposable
        {
            public new void Dispose()
            {
                base.Dispose();
                Result += " World!";
            }
        }
    }
}