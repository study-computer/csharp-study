﻿using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EffectiveCSharp.Test.Chapter4
{
    [TestClass]
    public class Item37
    {
        private static IEnumerable<int> GetAllIntegers()
        {
            var count = 0;

            while (count < int.MaxValue)
                yield return count++;
        }

        [TestMethod]
        public void LazyEvolution()
        {
            var integers = GetAllIntegers().Take(10);

            integers.Should().BeEquivalentTo(new[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9});
        }
    }
}