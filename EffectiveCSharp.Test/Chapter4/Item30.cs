﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EffectiveCSharp.Test.Chapter4
{
    [TestClass]
    public class Item30
    {
        [TestMethod]
        public void NestedForStatetment()
        {
            for (var x = 0; x < 100; x++)
            for (var y = 0; y < 100; y++)
                Console.WriteLine(x + y);
        }

        [TestMethod]
        public void BetterNestedRangeLinq()
        {
            var test = from x in Enumerable.Range(0, 100)
                from y in Enumerable.Range(0, 100)
                select (x, y);
        }

        [TestMethod]
        public void MethodStyleLinq()
        {
            var test = Enumerable.Range(0, 100)
                .SelectMany(x => Enumerable.Range(0, 100), (x, y) => (x, y))
                .Where(pt => pt.Item1 + pt.Item2 < 100)
                .OrderByDescending(pt => pt.Item1 * pt.Item1 + pt.Item2 * pt.Item2)
                .ToList();
        }
    }
}