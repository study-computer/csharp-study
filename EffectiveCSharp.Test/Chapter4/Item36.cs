﻿using System.Linq;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EffectiveCSharp.Test.Chapter4
{
    [TestClass]
    public class Item36
    {
        [TestMethod]
        public void SelectManyTest()
        {
            var odd = new[] {1, 3, 5, 7};
            var even = new[] {2, 4, 6, 8};

            var linqQuery = from oddNumber in odd
                from evenNumber in even
                select new
                {
                    oddNumber,
                    evenNumber,
                    Sum = oddNumber + evenNumber
                };

            var linqMethodChains = odd.SelectMany(oddNum => even,
                (oddNumber, evenNumber) => new {oddNumber, evenNumber, Sum = oddNumber + evenNumber});

            linqQuery.Should().BeEquivalentTo(linqMethodChains);
        }
    }
}