﻿using EffectiveCSharp.Chapter3;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EffectiveCSharp.Test.Chapter3
{
    [TestClass]
    public class Item18Test
    {
        [TestMethod]
        public void WhatReturnDefault()
        {
            Item18.ReturnDefault<int>().Should().Be(0);
            Item18.ReturnDefault<string>().Should().Be(null);
        }

        [TestMethod]
        public void WhatReturnNew()
        {
            var instance = Item18.ReturnNew<Item18TestClass>();

            instance.A.Should().Be("Hello world!");
        }

        [TestMethod]
        public void WhatReturnCreateInstance()
        {
            var instance = Item18.ReturnCreateInstance<Item18TestClass>();

            instance.A.Should().Be("Hello world!");
        }

        private class Item18TestClass
        {
            public Item18TestClass()
            {
                A = "Hello world!";
            }

            public string A { get; }
        }
    }
}