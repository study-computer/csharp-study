﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static EffectiveCSharp.Chapter3.Item20;

namespace EffectiveCSharp.Test.Chapter3
{
    [TestClass]
    public class Item20Test
    {
        [TestMethod]
        public void OverrideStandardOperatorTest()
        {
            var customer = new Customer("A", 0);
            var other = new Customer("B", 0);

            (customer <= other).Should().BeTrue();
        }
    }
}