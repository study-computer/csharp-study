using System;
using System.Reflection;
using Autofac;
using Autofac.Core.Registration;
using FluentAssertions;
using IoCContainer.Test.Context.Repositories;
using Xunit;

namespace IoCContainer.Test.AutoFac
{
    public class BasicInejctTest : IAdvancedInjectTest
    {
        [Fact]
        public void BatchAutomaticRegistrationTest()
        {
            var builder = new ContainerBuilder();

            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .Where(type => type.FullName.EndsWith("Repository"))
                .AsImplementedInterfaces();

            var container = builder.Build();

            container.Resolve<IARepository>().Should().NotBeNull();
            container.Resolve<IBRepository>().Should().NotBeNull();
        }

        [Fact]
        public void UnregistedTest()
        {
            var builder = new ContainerBuilder();

            var container = builder.Build();

            var action = new Action(() => container.Resolve<ARepository>());

            action.Should().Throw<ComponentNotRegisteredException>();
        }
    }
}