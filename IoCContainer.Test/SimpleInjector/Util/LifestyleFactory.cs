﻿using System.Collections.Generic;
using IoCContainer.Test.Infrastructure.Attributes;
using SimpleInjector;

namespace IoCContainer.Test.SimpleInjector.Util
{
    public class LifestyleFactory
    {
        private readonly IDictionary<LifestyleType, Lifestyle> _styles = new Dictionary<LifestyleType, Lifestyle>
        {
            {LifestyleType.Scope, Lifestyle.Scoped},
            {LifestyleType.Singleton, Lifestyle.Singleton},
            {LifestyleType.Transient, Lifestyle.Transient}
        };

        public Lifestyle GetLifestyle(LifestyleType type)
        {
            return _styles[type];
        }
    }
}