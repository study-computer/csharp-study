﻿using System.Linq;
using System.Reflection;
using FluentAssertions;
using IoCContainer.Test.Context.Components;
using IoCContainer.Test.Infrastructure.Attributes;
using IoCContainer.Test.SimpleInjector.Util;
using SimpleInjector;
using Xunit;

namespace IoCContainer.Test.SimpleInjector
{
    public class AttributeInjectTest
    {
        [Fact]
        public void should_return_component_instance_of_attribute()
        {
            var container = new Container();
            var factory = new LifestyleFactory();

            Assembly
                .GetExecutingAssembly()
                .GetExportedTypes()
                .Where(type => type.GetCustomAttributes<ComponentAttribute>().Any())
                .Where(type => type.GetInterfaces().Any())
                .Select(type => (
                    Interface: type.GetInterfaces().Single(),
                    Implemented: type,
                    Lifestyle: factory.GetLifestyle(type.GetCustomAttribute<ComponentAttribute>().LifestyleType)
                ))
                .ToList()
                .ForEach(meta => container.Register(meta.Interface, meta.Implemented, meta.Lifestyle));

            container.GetInstance<AComponent>().Should().NotBeNull();
        }
    }
}