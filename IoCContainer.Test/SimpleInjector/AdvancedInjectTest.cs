﻿using System.Linq;
using FluentAssertions;
using IoCContainer.Test.Context.Repositories;
using SimpleInjector;
using Xunit;

namespace IoCContainer.Test.SimpleInjector
{
    public class AdvancedInjectTest : IAdvancedInjectTest
    {
        public AdvancedInjectTest()
        {
            _container = new Container();
        }

        private readonly Container _container;

        [Fact]
        public void BatchAutomaticRegistrationTest()
        {
            typeof(ARepository).Assembly
                .GetExportedTypes()
                .Where(type => type.Name.EndsWith("Repository"))
                .Where(type => type.GetInterfaces().Any())
                .Select(type => new {Repository = type.GetInterfaces().Single(), Implementation = type})
                .ToList()
                .ForEach(reg => _container.Register(reg.Repository, reg.Implementation, Lifestyle.Transient));

            var aRepository = _container.GetInstance<IARepository>();
            var bRepository = _container.GetInstance<IBRepository>();

            aRepository.Should().NotBeNull();
            bRepository.Should().NotBeNull();
        }

        [Fact]
        public void UnregistedTest()
        {
            var container = new Container();

            var aRepository = container.GetInstance<ARepository>();

            aRepository.Should().NotBeNull();
        }
    }
}