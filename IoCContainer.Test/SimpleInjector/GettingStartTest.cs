using FluentAssertions;
using IoCContainer.Test.Context.Infrastructure;
using SimpleInjector;
using Xunit;

namespace IoCContainer.Test.SimpleInjector
{
    public class SampleComponent : ISampleComponent
    {
        public int Add(int a, int b)
        {
            return a + b;
        }
    }

    public interface ISampleComponent
    {
        int Add(int a, int b);
    }


    public class GettingStartTest
    {
        public GettingStartTest()
        {
            _container = new Container();
        }

        private readonly Container _container;

        [Fact]
        public void BasicInject()
        {
            _container.Register<ISampleComponent, SampleComponent>();
            _container.Verify();

            var component = _container.GetInstance<ISampleComponent>();

            component.Add(1, 2).Should().Be(3);
        }

        [Fact]
        public void LifestyeSingletonTest()
        {
            _container.Register<ISampleComponent, SampleComponent>(Lifestyle.Singleton);
            _container.Verify();

            var component1 = _container.GetInstance<ISampleComponent>();
            var component2 = _container.GetInstance<ISampleComponent>();

            component1.Should().Be(component2);
            component1.Should().BeEquivalentTo(component2);
        }

        [Fact]
        public void LifestyleScopeTset()
        {
            _container.Options.DefaultScopedLifestyle = new CustomScope("TestScope");
            _container.Register<ISampleComponent, SampleComponent>(Lifestyle.Scoped);
            _container.Verify();

            var component1 = _container.GetInstance<ISampleComponent>();
            var component2 = _container.GetInstance<ISampleComponent>();

            component1.Should().Be(component2);
            component1.Should().BeEquivalentTo(component2);
        }

        [Fact]
        public void LifestyleTransientTset()
        {
            _container.Register<ISampleComponent, SampleComponent>(Lifestyle.Transient);
            _container.Verify();

            var component1 = _container.GetInstance<ISampleComponent>();
            var component2 = _container.GetInstance<ISampleComponent>();

            component1.Should().NotBe(component2);
            component1.Should().NotBeSameAs(component2);
        }
    }
}