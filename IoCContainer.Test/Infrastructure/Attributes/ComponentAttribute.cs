﻿using System;

namespace IoCContainer.Test.Infrastructure.Attributes
{
    public class ComponentAttribute : Attribute
    {
        public ComponentAttribute(LifestyleType lifestyle)
        {
            LifestyleType = lifestyle;
        }

        public LifestyleType LifestyleType { get; set; }
    }

    public enum LifestyleType
    {
        Scope,
        Singleton,
        Transient
    }
}