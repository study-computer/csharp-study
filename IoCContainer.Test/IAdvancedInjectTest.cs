﻿namespace IoCContainer.Test
{
    public interface IAdvancedInjectTest
    {
        void BatchAutomaticRegistrationTest();
        void UnregistedTest();
    }
}