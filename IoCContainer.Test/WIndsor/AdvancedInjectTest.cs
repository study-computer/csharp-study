﻿using System;
using System.Reflection;
using Castle.MicroKernel;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using FluentAssertions;
using IoCContainer.Test.Context.Repositories;
using Xunit;
using static Castle.MicroKernel.Registration.AllTypes;

namespace IoCContainer.Test.WIndsor
{
    public class AdvancedInjectTest : IAdvancedInjectTest
    {
        [Fact]
        public void BatchAutomaticRegistrationTest()
        {
            var container = new WindsorContainer();

            container.Install(new RepositoryInstaller());

            container.Resolve<IARepository>().Should().NotBeNull();
            container.Resolve<IBRepository>().Should().NotBeNull();
        }

        [Fact]
        public void UnregistedTest()
        {
            var container = new WindsorContainer();

            var action = new Action(() => container.Resolve<ARepository>());

            action.Should().Throw<ComponentNotFoundException>();
        }
    }

    public class RepositoryInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(FromAssembly(Assembly.GetExecutingAssembly())
                .Where(type => type.Name.EndsWith("Repository"))
                .WithService.DefaultInterfaces()
                .Configure(c => c.LifestyleSingleton()));
        }
    }
}