using System;
using SimpleInjector;

namespace IoCContainer.Test.Context.Infrastructure
{
    public class CustomScope : ScopedLifestyle
    {
        private static readonly Scope Scope = new Scope();

        public CustomScope(string name) : base(name)
        {
        }

        protected override Func<Scope> CreateCurrentScopeProvider(Container container)
        {
            return () => Scope;
        }
    }
}