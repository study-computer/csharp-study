﻿using IoCContainer.Test.Infrastructure.Attributes;

namespace IoCContainer.Test.Context.Components
{
    [Component(LifestyleType.Singleton)]
    public class AComponent
    {
    }
}