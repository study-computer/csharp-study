using Xunit;

namespace EffectiveCSharp.Chapter1
{
    public class Item10
    {
        private object MakeObject()
        {
            return new NewOtherClass();
        }

        private class BaseClass
        {
            public virtual string Method()
            {
                return nameof(BaseClass);
            }
        }

        private class NewOtherClass : BaseClass
        {
            public new string Method()
            {
                return nameof(NewOtherClass);
            }
        }

        private class NewOverride : BaseClass
        {
            public override string Method()
            {
                return nameof(NewOverride);
            }
        }

        [Fact]
        public void NewKeyword()
        {
            var b = new NewOtherClass() as BaseClass;
            var o = new NewOverride() as BaseClass;

            Assert.Equal(b.Method(), nameof(BaseClass));
            Assert.Equal(o.Method(), nameof(NewOverride));
        }
    }
}