﻿using System;

namespace EffectiveCSharp.Chapter3
{
    public interface IEngine
    {
        void DoWork();
    }

    public class EngineDriver1<T> where T : IEngine, new()
    {
        public EngineDriver1()
        {
            var driver = new T();


            using (driver as IDisposable)
            {
                driver.DoWork();
            }
        }
    }

    public class EngineDrvier2<T> : IDisposable where T : IEngine, new()
    {
        private readonly Lazy<T> driver = new Lazy<T>(() => new T());


        public void Dispose()
        {
            if (driver is IDisposable d) d.Dispose();
        }

        public void GetThingsDone()
        {
            driver.Value.DoWork();
        }
    }
}