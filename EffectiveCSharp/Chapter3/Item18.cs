﻿using System;

namespace EffectiveCSharp.Chapter3
{
    public class Item18
    {
        public static T Factory<T>(Func<T> makeANewT) where T : new()
        {
            var newT = makeANewT();

            return newT != null ? newT : new T();
        }

        public static T ReturnDefault<T>()
        {
            return default(T);
        }

        public static T ReturnNew<T>() where T : new()
        {
            return new T();
        }

        public static T ReturnCreateInstance<T>() where T : new()
        {
            return Activator.CreateInstance<T>();
        }
    }
}