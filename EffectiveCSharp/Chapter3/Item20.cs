﻿using System;
using System.Collections.Generic;

namespace EffectiveCSharp.Chapter3
{
    public class Item20
    {
        public struct Customer : IComparable<Customer>, IComparable
        {
            private readonly string _name;
            private readonly double _revenue;

            public Customer(string name, double revenue)
            {
                _name = name;
                _revenue = revenue;
            }

            public int CompareTo(Customer other)
            {
                return string.Compare(_name, other._name, StringComparison.Ordinal);
            }

            public int CompareTo(object obj)
            {
                return obj is Customer other
                    ? CompareTo(other)
                    : throw new ArgumentException("Argument is not a Customer", nameof(obj));
            }

            public static bool operator <(Customer left, Customer right)
            {
                return left.CompareTo(right) < 0;
            }

            public static bool operator >(Customer left, Customer right)
            {
                return left.CompareTo(right) > 0;
            }

            public static bool operator <=(Customer left, Customer right)
            {
                return left.CompareTo(right) <= 0;
            }

            public static bool operator >=(Customer left, Customer right)
            {
                return left.CompareTo(right) >= 0;
            }

            private static readonly Lazy<RevenueComparer> _revComp =
                new Lazy<RevenueComparer>(() => new RevenueComparer());

            public Customer(string name)
            {
                _name = name;
                _revenue = 0;
            }

            public static IComparer<Customer> RevenueCompare => _revComp.Value;

            public static Comparison<Customer> CompareByRevenue =>
                (left, right) => left._revenue.CompareTo(right._revenue);

            private class RevenueComparer : IComparer<Customer>
            {
                public int Compare(Customer left, Customer right)
                {
                    return left._revenue.CompareTo(right._revenue);
                }
            }
        }
    }
}