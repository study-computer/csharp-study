﻿using Moq;
using Xunit;

namespace EffectiveCSharp.Chapter2
{
    public class Item16
    {
        public interface IConsole
        {
            void WriteLine(string message);
        }

        private class B
        {
            protected B(IConsole console)
            {
                console.WriteLine(VirtualMethod());
            }

            public virtual string VirtualMethod()
            {
                return "Parent Virtual Function";
            }
        }

        private class Derived : B
        {
            private readonly string _message = "Set by initializer";

            public Derived(string message, IConsole console) : base(console)
            {
                _message = message;
            }

            public override string VirtualMethod()
            {
                return _message;
            }
        }

        [Fact]
        public void WhatReturn()
        {
            var console = new Mock<IConsole>();
            console.Setup(c => c.WriteLine(It.IsAny<string>()));

            var result = new Derived("Test Method", console.Object).VirtualMethod();

            console.Verify(c => c.WriteLine("Set by initializer"));
            Assert.Equal("Test Method", result);
        }
    }
}